# Accessing Data with JPA   

This guide walks you through the process of building an application that uses Spring Data JPA to store and retrieve data in a relational database.

## What you’ll build

You’ll build an application that stores Customer POJOs in a memory-based database.

## Define a simple entity

In this example, you store Customer objects, annotated as a JPA entity.

    src/main/java/hello/Customer.java
                                          
    package hello;
    
    import javax.persistence.Entity;
    import javax.persistence.GeneratedValue;
    import javax.persistence.GenerationType;
    import javax.persistence.Id;
    
    @Entity
    public class Customer {
    
        @Id
        @GeneratedValue(strategy=GenerationType.AUTO)
        private Long id;
        private String firstName;
        private String lastName;
    
        protected Customer() {}
    
        public Customer(String firstName, String lastName) {
            this.firstName = firstName;
            this.lastName = lastName;
        }
    
        @Override
        public String toString() {
            return String.format(
                    "Customer[id=%d, firstName='%s', lastName='%s']",
                    id, firstName, lastName);
        }
    
    }

Here you have a Customer class with three attributes, id, firstName, and lastName. The id is mostly for internal use by MongoDB. You also have a single constructor to populate the entities when creating a new instance.

id fits the standard name for a MongoDB id so it doesn’t require any special annotation to tag it for Spring Data MongoDB.

The other two properties, firstName and lastName, are left unannotated. It is assumed that they’ll be mapped to fields that share the same name as the properties themselves.

The convenient toString() method will print out the details about a customer.

## Create simple queries

Spring Data MongoDB focuses on storing data in MongoDB. It also inherits functionality from the Spring Data Commons project, such as the ability to derive queries. Essentially, you don’t have to learn the query language of MongoDB; you can simply write a handful of methods and the queries are written for you.

To see how this works, create a repository interface that queries Customer documents.

    src/main/java/hello/CustomerRepository.java

    package hello;
    
    import java.util.List;
    
    import org.springframework.data.mongodb.repository.MongoRepository;
    
    public interface CustomerRepository extends MongoRepository<Customer, String> {
    
        public Customer findByFirstName(String firstName);
        public List<Customer> findByLastName(String lastName);
    
    }

CustomerRepository extends the MongoRepository interface and plugs in the type of values and id it works with: Customer and String. Out-of-the-box, this interface comes with many operations, including standard CRUD operations (create-read-update-delete).

You can define other queries as needed by simply declaring their method signature. In this case, you add findByFirstName, which essentially seeks documents of type Customer and finds the one that matches on firstName.

You also have findByLastName to find a list of people by last name.

In a typical Java application, you write a class that implements CustomerRepository and craft the queries yourself. What makes Spring Data MongoDB so useful is the fact that you don’t have to create this implementation. Spring Data MongoDB creates it on the fly when you run the application.

Let’s wire this up and see what it looks like!

## Create an Application class

Here you create an Application class with all the components.

    src/main/java/hello/Application.java
    
    package hello;
    
    import org.springframework.beans.factory.annotation.Autowired;
    import org.springframework.boot.CommandLineRunner;
    import org.springframework.boot.SpringApplication;
    import org.springframework.boot.autoconfigure.SpringBootApplication;
    
    @SpringBootApplication
    public class Application implements CommandLineRunner {
    
    	@Autowired
    	private CustomerRepository repository;
    
    	public static void main(String[] args) {
    		SpringApplication.run(Application.class, args);
    	}
    
    	@Override
    	public void run(String... args) throws Exception {
    
    		repository.deleteAll();
    
    		// save a couple of customers
    		repository.save(new Customer("Alice", "Smith"));
    		repository.save(new Customer("Bob", "Smith"));
    
    		// fetch all customers
    		System.out.println("Customers found with findAll():");
    		System.out.println("-------------------------------");
    		for (Customer customer : repository.findAll()) {
    			System.out.println(customer);
    		}
    		System.out.println();
    
    		// fetch an individual customer
    		System.out.println("Customer found with findByFirstName('Alice'):");
    		System.out.println("--------------------------------");
    		System.out.println(repository.findByFirstName("Alice"));
    
    		System.out.println("Customers found with findByLastName('Smith'):");
    		System.out.println("--------------------------------");
    		for (Customer customer : repository.findByLastName("Smith")) {
    			System.out.println(customer);
    		}
    
    	}
    
    }

@SpringBootApplication is a convenience annotation that adds all of the following:

@Configuration tags the class as a source of bean definitions for the application context.

@EnableAutoConfiguration tells Spring Boot to start adding beans based on classpath settings, other beans, and various property settings.

Normally you would add @EnableWebMvc for a Spring MVC app, but Spring Boot adds it automatically when it sees spring-webmvc on the classpath. This flags the application as a web application and activates key behaviors such as setting up a DispatcherServlet.

@ComponentScan tells Spring to look for other components, configurations, and services in the the hello package, allowing it to find the HelloController.

The main() method uses Spring Boot’s SpringApplication.run() method to launch an application. Did you notice that there wasn’t a single line of XML? No web.xml file either. This web application is 100% pure Java and you didn’t have to deal with configuring any plumbing or infrastructure.

Spring Boot will handle those repositories automatically as long as they are included in the same package (or a sub-package) of your @SpringBootApplication class. For more control over the registration process, you can use the @EnableMongoRepositories annotation.

Spring Data MongoDB uses the MongoTemplate to execute the queries behind your find* methods. You can use the template yourself for more complex queries, but this guide doesn’t cover that.

Application includes a main() method that autowires an instance of CustomerRepository: Spring Data MongoDB dynamically creates a proxy and injects it there. We use the CustomerRepository through a few tests. First, it saves a handful of Customer objects, demonstrating the save() method and setting up some data to work with. Next, it calls findAll() to fetch all Customer objects from the database. Then it calls findByFirstName() to fetch a single Customer by her first name. Finally, it calls findByLastName() to find all customers whose last name is "Smith".

As our Application implements CommandLineRunner, the run method is invoked automatically when boot starts. You should see something like this (with other stuff like queries as well):

    == Customers found with findAll():
    Customer[id=51df1b0a3004cb49c50210f8, firstName='Alice', lastName='Smith']
    Customer[id=51df1b0a3004cb49c50210f9, firstName='Bob', lastName='Smith']
    
    == Customer found with findByFirstName('Alice'):
    Customer[id=51df1b0a3004cb49c50210f8, firstName='Alice', lastName='Smith']
    == Customers found with findByLastName('Smith'):
    Customer[id=51df1b0a3004cb49c50210f8, firstName='Alice', lastName='Smith']
    Customer[id=51df1b0a3004cb49c50210f9, firstName='Bob', lastName='Smith']

## Summary

You set up a MongoDB server and wrote a simple application that uses Spring Data MongoDB to save objects to and fetch them from a database — all without writing a concrete repository implementation.